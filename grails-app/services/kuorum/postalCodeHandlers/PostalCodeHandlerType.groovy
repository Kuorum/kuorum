package kuorum.postalCodeHandlers

/**
 * Created by iduetxe on 2/05/15.
 */
public enum PostalCodeHandlerType {

    STANDARD_FIVE_DIGITS,
    STANDARD_SIX_DIGITS,
    UK
}