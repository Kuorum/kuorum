
var dbDest = dbDest || connect("localhost:27017/Kuorum");

dbDest.kuorumUser.find().forEach(function(user) {
    if (user.avatar != undefined && user.avatar.url.indexOf("http://kuorum.org")>=0){
        var URL = user.avatar.url.substring(7);
        URL = "https://"+URL;
        print(URL);
        dbDest.kuorumUser.update({_id:user._id},{$set:{'avatar.url':URL}})
    }
})
dbDest.kuorumUser.find({'avatar.url':/facebook/}).forEach(function(user) {
    if (user.avatar != undefined && user.avatar.url.indexOf("http://graph.facebook.com")>=0){
        var URL = user.avatar.url.substring(7);
        URL = "https://"+URL;
        print(URL);
        dbDest.kuorumUser.update({_id:user._id},{$set:{'avatar.url':URL}})
    }
})

dbDest.kuorumUser.find({imageProfile:{$exists:1}}).forEach(function(user) {
    if (user.imageProfile != undefined && user.imageProfile.url.indexOf("http://kuorum.org")>=0){
        var URL = user.imageProfile.url.substring(7);
        URL = "https://"+URL;
        print(URL);
        dbDest.kuorumUser.update({_id:user._id},{$set:{'imageProfile.url':URL}})
    }
})

dbDest.project.find({image:{$exists:1}}).forEach(function(project) {
    if (project.image != undefined && project.image.url.indexOf("http://kuorum.org")>=0){
        var URL = project.image.url.substring(7);
        URL = "https://"+URL;
        print(URL);
        dbDest.project.update({_id:project._id},{$set:{'image.url':URL}})
    }
})

dbDest.post.find({multimedia:{$exists:1}}).forEach(function(post) {
    if (post.multimedia != undefined && post.multimedia.url.indexOf("http://kuorum.org")>=0){
        var URL = post.multimedia.url.substring(7);
        URL = "https://"+URL;
        print(URL);
        dbDest.post.update({_id:post._id},{$set:{'multimedia.url':URL}})
    }
})

dbDest.kuorumFile.find({url:/^http:/}).forEach(function(file) {
    if (file.url != undefined && file.url.indexOf("http://")>=0){
        var URL = file.url.substring(7);
        URL = "https://"+URL;
        print(URL);
        dbDest.kuorumFile.update({_id:file._id},{$set:{'url':URL}})
    }
})