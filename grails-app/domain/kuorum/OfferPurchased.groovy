package kuorum

import kuorum.core.model.OfferType
import kuorum.users.KuorumUser

/**
 * Created by iduetxe on 7/03/15.
 */
class OfferPurchased {
    OfferType offerType
    KuorumUser user
    Date dateCreated
}
