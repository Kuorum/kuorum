package kuorum.notifications

/**
 * Represents a notification when someone writes a comment on a post that the user already wrote a comment
 */
class CommentGenericNotification extends CommentNotification{
    static constraints = {
    }
}