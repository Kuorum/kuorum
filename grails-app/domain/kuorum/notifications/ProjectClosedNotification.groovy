package kuorum.notifications

import kuorum.project.Project

class ProjectClosedNotification extends  Notification{

    Project project

    static constraints = {
    }
}
