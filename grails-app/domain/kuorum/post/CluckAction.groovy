package kuorum.post

/**
 * Created by iduetxe on 26/10/14.
 */
public enum CluckAction {
    CREATE,
    CLUCK,
    SPONSOR,
    DEBATE,
    DEFEND,
    VICTORY;
}