<section id="main" class="col-xs-12 col-md-7" role="main">
    <ul class="video-embudo-list">
        <li>
            <span class="fa icon2-comillas fa-4x"></span>
            <div class="vimeo uno">
                <a href="#" class="front">
                    <span class="fa fa-play-circle fa-4x"></span>
                    <img src="images/embudo-porcausa.jpg">
                    <span class="text"><g:message code="funnel.successfulStories.vimeo1.description"/></span>
                    <span class="who"><g:message code="funnel.successfulStories.vimeo1.author"/><br/><small><g:message code="funnel.successfulStories.vimeo1.position"/></small></span>
                </a>
                <iframe id="vimeoplayer1" class="youtube" src="//player.vimeo.com/video/119323867?api=1&amp;player_id=vimeoplayer1&amp;autoplay=0&amp;color=ff9933&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </li>
        <li>
            <span class="fa icon2-comillas fa-4x"></span>
            <div class="vimeo dos">
                <a href="#" class="front">
                    <span class="fa fa-play-circle fa-4x"></span>
                    <img src="images/embudo-campuzano.jpg">
                    <span class="text"><g:message code="funnel.successfulStories.vimeo2.description"/></span>
                    <span class="who"><g:message code="funnel.successfulStories.vimeo2.author"/><br/><small><g:message code="funnel.successfulStories.vimeo2.position"/></small></span>
                </a>
                <iframe id="vimeoplayer2" class="youtube" src="//player.vimeo.com/video/119323868?api=1&amp;player_id=vimeoplayer2&amp;autoplay=0&amp;color=ff9933&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </li>
        <li>
            <span class="fa icon2-comillas fa-flip-horizontal fa-4x"></span>
            <div class="vimeo tres">
                <a href="#" class="front">
                    <span class="fa fa-play-circle fa-4x"></span>
                    <img src="images/embudo-cuenta.jpg">
                    <span class="text"><g:message code="funnel.successfulStories.vimeo3.description"/></span>
                    <span class="who"><g:message code="funnel.successfulStories.vimeo3.author"/><br/><small><g:message code="funnel.successfulStories.vimeo3.position"/></small></span>
                </a>
                <iframe id="vimeoplayer3" class="youtube" src="//player.vimeo.com/video/119323869?api=1&amp;player_id=vimeoplayer3&amp;autoplay=0&amp;color=ff9933&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </li>

    </ul>
</section>
