<div role="tabpanel">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="noTengoCuenta">
            <g:form mapping="funnelPaySuccess" role="form" class="form-inline funnelFormDetectUser" name="login-comprar" method="post">
                <input type="hidden" name="offerType" value="${offerType}"/>
                <div class="form-group">
                    <input type="submit" value="${g.message(code:'funnel.payment.button')}" class="btn btn-lg">
                </div>
            </g:form>
        </div>
    </div>
</div>