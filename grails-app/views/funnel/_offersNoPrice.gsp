    <section class="pide-cuenta">
        <h1><g:message code="funnel.successfulStories.offers.title"/></h1>
        <h2><g:message code="funnel.successfulStories.offers.subtitle"/></h2>
        <ul class="clearfix">
            <li class="col-sm-4">
                <span><g:message code="funnel.successfulStories.offers.basic.name"/></span>
                <ul>
                    <li><g:message code="funnel.successfulStories.offers.basic.1"/></li>
                    <li><g:message code="funnel.successfulStories.offers.basic.2"/></li>
                    <li><g:message code="funnel.successfulStories.offers.basic.3"/></li>
                </ul>
            </li>
            <li class="col-sm-4">
                <span><g:message code="funnel.successfulStories.offers.premium.name"/></span>
                <ul>
                    <li><g:message code="funnel.successfulStories.offers.premium.1"/></li>
                    <li><g:message code="funnel.successfulStories.offers.premium.2"/></li>
                    <li><g:message code="funnel.successfulStories.offers.premium.3"/></li>
                </ul>
            </li>
            <li class="col-sm-4">
                <span><g:message code="funnel.successfulStories.offers.cityHall.name"/></span>
                <ul>
                    <li><g:message code="funnel.successfulStories.offers.cityHall.1"/></li>
                    <li><g:message code="funnel.successfulStories.offers.cityHall.2"/></li>
                    <li><g:message code="funnel.successfulStories.offers.cityHall.3"/></li>
                </ul>
            </li>
        </ul>
        <div class="embudoMore">
    <g:link mapping="funnelOffers" class="btn btn-lg btn-block"><g:message code="funnel.successfulStories.offers.button"/></g:link>
        </div>
    </section>