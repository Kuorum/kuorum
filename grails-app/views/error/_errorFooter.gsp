<br>
<p style="text-align:center;"><strong>${title}</strong></p>
%{--<p><strong>No hemos encontrado la página que buscabas</strong></p>--}%
<br/>
<div style="align:center; text-align:center;">
<g:link mapping="home" class="btn btn-lg">
    <g:message code="error.genericFooter.backHome"/>
</g:link>
</div>
<br/>
<p></p>
%{--<g:link mapping="projects" class="btn btn-lg btn-blue">--}%
    %{--<g:message code="error.genericFooter.showProjects"/>--}%
%{--</g:link>--}%