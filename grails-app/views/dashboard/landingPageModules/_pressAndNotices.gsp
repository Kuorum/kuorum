<section role="complementary" class="homeSub medios">
    <div class="row">
        <h1><g:message code="landingPage.pressAndNotices.press.pressTitle"/></h1>
        <ul class="medios-list clearfix">
            <li class="col-sm-4">
                <blockquote itemscope itemtype="http://schema.org/Review">
                    <footer>
                        <cite itemprop="publisher">
                            <img src="images/logo-eldiario.png" alt="${g.message(code: 'landingPage.pressAndNotices.press.elDiario.imageAlt')}">
                        </cite>
                    </footer>
                    <span itemprop="itemReviewed" class="sr-only">Kuorum.org</span>
                    <p itemprop="reviewBody"><g:message code="landingPage.pressAndNotices.press.elDiario.review"/></p>
                </blockquote>
            </li>
            <li class="col-sm-4">
                <blockquote itemscope itemtype="http://schema.org/Review">
                    <footer>
                        <cite itemprop="publisher">
                            <img src="images/logo-rne.png" alt="Radio Nacional de España">
                        </cite>
                    </footer>
                    <span itemprop="itemReviewed" class="sr-only">Kuorum.org</span>
                    <p itemprop="reviewBody"><g:message code="landingPage.pressAndNotices.press.rne.review"/></p>
                </blockquote>
            </li>
            <li class="col-sm-4">
                <blockquote itemscope itemtype="http://schema.org/Review">
                    <footer>
                        <cite itemprop="publisher">
                            <img src="images/logo-elperiodico.png" alt="el Periódico">
                        </cite>
                    </footer>
                    <span itemprop="itemReviewed" class="sr-only">Kuorum.org</span>
                    <p itemprop="reviewBody"><g:message code="landingPage.pressAndNotices.press.elPeriodico.review"/></p>
                </blockquote>
            </li>
        </ul>
        <ul class="medios-list clearfix">
            <li class="col-sm-4">
                <blockquote itemscope itemtype="http://schema.org/Review">
                    <footer>
                        <cite itemprop="publisher">
                            <img src="images/logo-elconfidencial.png" alt="${message(code: 'landingPage.pressAndNotices.press.elconfidencial.imageAlt')}">
                        </cite>
                    </footer>
                    <span itemprop="itemReviewed" class="sr-only">Kuorum.org</span>
                    <p itemprop="reviewBody"><g:message code="landingPage.pressAndNotices.press.elconfidencial.review"/></p>
                </blockquote>
            </li>
            <li class="col-sm-4">
                <blockquote itemscope itemtype="http://schema.org/Review">
                    <footer>
                        <cite itemprop="publisher">
                            <img src="images/logo-lados.png" alt="La 2. Televisión Española.">
                        </cite>
                    </footer>
                    <span itemprop="itemReviewed" class="sr-only">Kuorum.org</span>
                    <p itemprop="reviewBody"><g:message code="landingPage.pressAndNotices.press.tve2.review"/></p>
                </blockquote>
            </li>
            <li class="col-sm-4">
                <blockquote itemscope itemtype="http://schema.org/Review">
                    <footer>
                        <cite itemprop="publisher">
                            <img src="images/logo-lainformacion.png" alt="lainformacion.com">
                        </cite>
                    </footer>
                    <span itemprop="itemReviewed" class="sr-only">Kuorum.org</span>
                    <p itemprop="reviewBody"><g:message code="landingPage.pressAndNotices.press.laInformacion.review"/></p>
                </blockquote>
            </li>
        </ul>
        <h2><g:message code="landingPage.pressAndNotices.supportedBy.title"/> </h2>
        <ul class="support-list">
            <li class="game"><img src="images/logo-game.jpg" alt="Game Changers"></li>
            <li class="unltdspain"><img src="images/logo-unltdspain.jpg" alt="UNLTD Spain"></li>
            <li class="iebusiness"><img src="images/logo-iebusiness.jpg" alt="IE Business School"></li>
        </ul>
    </div>
</section>