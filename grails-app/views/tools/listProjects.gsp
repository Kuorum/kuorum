<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <title><g:message code="project.list.title"/> </title>
    <meta name="layout" content="leftMenuLayout">
    <parameter name="extraCssContainer" value="config" />
</head>

<content tag="leftMenu">
    <g:render template="leftMenu" model="[user:user, activeMapping:'projectList', menu:menu]"/>
</content>

<content tag="mainContent">
    <div id="projectsList">
        <g:render template="projects" model="[projects:projects, order: order, sort: sort, published: published,
                max: max, offset: offset, totalProjects: totalProjects, publishedProjects: publishedProjects,
                draftProjects: draftProjects, seeMore: seeMore]"/>
    </div>
</content>
