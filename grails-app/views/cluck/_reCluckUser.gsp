<div class="user actor" itemscope itemtype="http://schema.org/Person">
    <userUtil:showUser user="${cluck.owner}"/>
    <span class="action">
        <small>
            <g:message code="cluck.header.action.${cluck.cluckAction}"/>
            <kuorumDate:humanDate date="${cluck.lastUpdated}"/>
        </small>
    </span>
</div><!-- / persona que kakarea -->