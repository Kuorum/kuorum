<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <title><g:message code="page.title.login"/> </title>
    <meta name="layout" content="register2ColumnsLayout">
</head>

<content tag="headButtons">
    <g:include controller="login" action="headAuth"/>
</content>

<content tag="intro">
    <h1><g:message code="forgotPasswordSuccess.intro.login"/> </h1>
    <p><g:message code="forgotPasswordSuccess.intro.description"/></p>
</content>

<content tag="mainContent">

</content>

<content tag="description">
    <h1><g:message code="forgotPasswordSuccess.intro.title"/> </h1>
    <h2><g:message code="forgotPasswordSuccess.intro.subTitle"/> </h2>
</content>