<ul class="noLoggedVoteDiv">
    <li>
        <a role="button" data-toggle="modal" data-target="#registro" href="#">
            <span class="icon-smiley ${cssIconSize}"></span>
            <span class="${header?'sr-only':''}"><g:message code="project.vote.yes"/></span>
        </a>
    </li>
    <li>
        <a role="button" data-toggle="modal" data-target="#registro" href="#">
            <span class="icon-sad ${cssIconSize}"></span>
            <span class="${header?'sr-only':''}"><g:message code="project.vote.no"/></span>
        </a>
    </li>
    <li>
        <a role="button" data-toggle="modal" data-target="#registro" href="#">
            <span class="icon-neutral ${cssIconSize}"></span>
            <span class="${header?'sr-only':''}"><g:message code="project.vote.abs"/></span>
        </a>
    </li>
    <li>
        <a role="button" data-toggle="modal" data-target="#registro" href="#" class="design">
            <span class="fa fa-lightbulb-o ${cssIconSize}"></span>
            <span class="${header?'sr-only':''}"><g:message code="project.vote.newPost"/></span>
        </a>
    </li>
</ul>