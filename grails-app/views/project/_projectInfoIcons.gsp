<div class="col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9 col-lg-2 col-lg-pull-10">
    <ul class="icons">
        <g:if test="${project.pdfFile}">
            <li>
                <span class="sr-only"><g:message code="project.downloadPDF"/></span>
                <a href="${project.pdfFile.url}" target="_blank"><span class="fa fa-file-pdf-o fa-2x"></span></a>
            </li>
        </g:if>
        %{--<li>--}%
            %{--<span class="sr-only">Abrir código widget para compartir proyecto</span>--}%
            %{--<a href="#" data-toggle="modal" data-target="#widget">--}%
                %{--<span class="fa-stack fa-lg">--}%
                    %{--<span class="fa fa-circle fa-stack-2x"></span>--}%
                    %{--<span class="fa fa-code fa-stack-1x fa-inverse"></span>--}%
                %{--</span>--}%
            %{--</a>--}%
        %{--</li>--}%
    </ul>
    <!-- Modal widget -->
    <div class="modal fade" id="widget" tabindex="-1" role="dialog" aria-labelledby="widgetCompartir" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle-o fa"></span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title" id="widgetCompartir">Widget del proyecto político</h4>
                </div>
                <div class="modal-body">
                    <p>Para compartir este proyecto en su web o en su blog copie este código en el html de su página:</p>
                    <pre class="pre-scrollable">&lt;iframe src="http://www.votenaweb.com.br/projetos/plc-8152-2014/widget" frameborder="0" width="311" height="679" scrolling="no" marginheight="0" marginwidth="0"&gt;&lt;/iframe&gt;</pre>
                </div>
            </div>
        </div>
    </div>
    <!-- fin modal widget -->

    <ul class="icons subject">
        <!-- si estás logado cuando haces clic el <a> debe adquirir la clase active -->
        <g:each in="${project.commissions}" var="commission">
            <li>
                <span class="sr-only"><g:message code="kuorum.core.model.CommissionType.${commission}"/> </span>
                <a href="#">
                    <span class="icon-${commission} fa-2x"></span>
                </a>
            </li>
        </g:each>
    </ul>
</div>