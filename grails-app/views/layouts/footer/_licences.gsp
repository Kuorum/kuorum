<ul class="licenses clearfix">
    <li><span class="sr-only"><g:message code="layout.footer.licences"/></span></li>
    <li>
        <a href="https://github.com/Kuorum/kuorum" hreflang="en" target="_blank">
            <span class="sr-only">Github</span>
            <span class="fa fa-github fa-3x"></span>
        </a>
    </li>
    <li>
        <a class="agpl" hreflang="en" href="http://www.gnu.org/licenses/agpl-3.0.html" target="_blank">AGPL (Affero General Public License)</a>
    </li>
</ul>