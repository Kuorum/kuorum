<footer id="footer" class="row" role="contentinfo">
    <section class="more">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <section class="links">
                        <ul>
                            <li><g:link mapping="footerPrivacyPolicy" target="_blank"><g:message code="layout.footer.privacyPolicy"/></g:link></li>
                            <li><g:link mapping="footerTermsUse" target="_blank"><g:message code="layout.footer.termsUse"/></g:link></li>
                        </ul>
                    </section>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <g:render template="/layouts/footer/licences"/>
                </div>
            </div>
        </div><!-- /.container-fluid - da ancho máximo y centra -->
    </section>
</footer>
