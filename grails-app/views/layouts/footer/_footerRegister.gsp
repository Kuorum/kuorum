<g:include controller="modules" action="registerFooterRelevantUsers"/>

<footer id="footer" class="row" role="contentinfo">
    <div class="container-fluid">
        <section class="links">
            <ul>
                <li><g:link mapping="footerPrivacyPolicy"><g:message code="layout.footer.privacyPolicy"/></g:link></li>
                <li><g:link mapping="footerTermsUse"><g:message code="layout.footer.termsUse"/></g:link></li>
            </ul>
        </section>
    </div><!-- /.container-fluid - da ancho máximo y centra -->

    <section class="more">
        <div class="container-fluid">
            <div class="row">
                %{--<div class="col-xs-12 col-sm-8 col-md-8">--}%
                    %{--<ul class="social">--}%
                        %{--<li class="hidden-xs"><g:message code="layout.footer.followUs"/></li>--}%
                        %{--<li><a href="https://twitter.com/kuorumorg" target="_blank"><span class="sr-only">Twitter</span><span class="fa-stack fa-lg"><span class="fa fa-circle fa-stack-2x"></span><span class="fa fa-twitter fa-stack-1x"></span></span></a></li>--}%
                        %{--<li><a href="https://www.facebook.com/kuorumorg" target="_blank"><span class="sr-only" target="_blank">Facebook</span><span class="fa-stack fa-lg"><span class="fa fa-circle fa-stack-2x"></span><span class="fa fa-facebook fa-stack-1x"></span></span></a></li>--}%
                        %{--<li><a href="http://www.linkedin.com/company/kuorum" target="_blank"><span class="sr-only" target="_blank">LinkedIn</span><span class="fa-stack fa-lg"><span class="fa fa-circle fa-stack-2x"></span><span class="fa fa-linkedin fa-stack-1x"></span></span></a></li>--}%
                        %{--<li><a href="https://plus.google.com/+KuorumOrg" target="_blank"><span class="sr-only" target="_blank">Google+</span><span class="fa-stack fa-lg"><span class="fa fa-circle fa-stack-2x"></span><span class="fa fa-google-plus fa-stack-1x"></span></span></a></li>--}%
                    %{--</ul>--}%
                %{--</div>--}%
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <g:render template="/layouts/footer/licences"/>
                </div>
            </div>
        </div><!-- /.container-fluid - da ancho máximo y centra -->
    </section>
</footer>
