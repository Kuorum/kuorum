<section class="boxes follow">
    <h1>Usuarios relacionados</h1>
    <g:render template="/modules/users/recommendedUsersAsList" model="[users:relatedUsers, showDeleteRecommendation:'false']"/>
</section>