package kuorum.post

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import kuorum.KuorumFile
import kuorum.core.FileGroup
import kuorum.core.FileType
import kuorum.core.model.CommitmentType
import kuorum.core.model.PostType
import kuorum.core.model.UserType
import kuorum.core.model.VoteType
import kuorum.core.model.gamification.GamificationElement
import kuorum.core.model.search.Pagination
import kuorum.files.FileService
import kuorum.project.Project
import kuorum.users.KuorumUser
import kuorum.web.commands.PostCommand
import kuorum.web.commands.post.CommentPostCommand
import kuorum.web.commands.post.PostDefendCommand
import kuorum.web.commands.post.PromotePostCommand
import kuorum.web.constants.WebConstants
import org.bson.types.ObjectId
import springSecurity.KuorumRegisterCommand

import javax.servlet.http.HttpServletResponse

class PostController {

    def postService
    def postVoteService
    def springSecurityService
    def projectService
    def cluckService
    def gamificationService
    def grailsApplication
    def fileService
    def registerService

    def index() {
        [postInstanceList:Post.list()]
    }

    def show(){
        Post post = params.post //Intellij Detects type for autocomplete
        KuorumUser user= null
        PostVote userVote=null
        if (springSecurityService.isLoggedIn()){
            user = KuorumUser.get(springSecurityService.principal.id)
            userVote = PostVote.findByPostAndUser(post,user)
        }
        List<Post> relatedPost = postService.relatedPosts(post,  user,  3 )
        List<KuorumUser> usersVotes = postVoteService.findVotedUsers(post, new Pagination(max:20))

        def model = [post:post,relatedPost:relatedPost, usersVotes:usersVotes, userVote:userVote]
        model
    }

    @Secured(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PREMIUM', 'ROLE_POLITICIAN'])
    def edit(){
        Post post = params.post
        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        if (!postService.isEditableByUser(post, user)){
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
            return;
        }
        PostCommand command = new PostCommand()
        command.postId = post.id.toString()
        command.imageId = post.multimedia?.fileType == FileType.IMAGE?post.multimedia.id:null
        command.videoPost = post.multimedia?.fileType == FileType.YOUTUBE?post.multimedia.url:''
        command.fileType = post.multimedia?.fileType
        command.textPost = post.text
        command.title = post.title
        [command:command,post:post ]
    }

    @Secured(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PREMIUM', 'ROLE_POLITICIAN'])
    def update(PostCommand command){
        Post post = params.post
        boolean wasPublised = post.published
        if (!post){
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return;
        }
        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        if (!postService.isEditableByUser(post, user)){
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
            return;
        }
        if (command.hasErrors()){
            render view: "edit", model:[command:command,post:post ]
            return
        }
        post.multimedia = preparePostFile(post, command,user)
        post.text = command.textPost
        post.title = command.title

        postService.updatePost(post)
        if (command.isDraft){
            flash.message = message(code:"post.edit.step1.saveDraft.success")
            redirect mapping:"toolsMyPosts"
        }else{
            if (!wasPublised){
                postService.publishPost(post);
            }
            flash.message = message(code:"post.edit.step1.update.success")
            redirect mapping:"postShow", params:post.encodeAsLinkProperties()
        }
    }

    private KuorumFile preparePostFile(Post post, PostCommand command, KuorumUser user){
        KuorumFile multimedia = null
        if (!command.fileType){
            multimedia = null
        }else if (command.fileType == FileType.IMAGE && command.imageId){
            multimedia = KuorumFile.get(new ObjectId(command.imageId))
        }else if(command.fileType == FileType.YOUTUBE && command.videoPost && !post.multimedia){
            multimedia = fileService.createYoutubeKuorumFile(command.videoPost, user)
        }else if (command.fileType == FileType.YOUTUBE && command.videoPost && post.multimedia){
            if (command.videoPost == post.multimedia.url){
                multimedia = KuorumFile.get(post.multimedia.id)
            }else{
                fileService.deleteFile(post.multimedia)
                multimedia = fileService.createYoutubeKuorumFile(command.videoPost, user)
            }
        }
        multimedia
    }

    @Secured(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PREMIUM', 'ROLE_POLITICIAN'])
    def create(String hashtag){
        Project project = projectService.findProjectByHashtag(hashtag.encodeAsHashtag())
        if (!project){
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return;
        }
        PostCommand command = new PostCommand()
        [command:command, project:project]
    }

    @Secured(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PREMIUM', 'ROLE_POLITICIAN'])
    def save(PostCommand command){
        Project project = projectService.findProjectByHashtag(params.hashtag.encodeAsHashtag())
        if (!project){
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return
        }
        if (command.hasErrors()){
            render view: "create", model:[command:command,project:project]
            return;
        }

        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        Post post = new Post()

        post.multimedia = preparePostFile(post, command, user)
        post.text = command.textPost
        post.title = command.title

        postService.savePost(post, project, user)
        if (command.isDraft){
            flash.message = message(code:"post.edit.step1.saveDraft.success")
            redirect mapping:"toolsMyPosts"
        }else{
            postService.publishPost(post)
            flash.message = message(code:"post.edit.step1.save.success")
            redirect mapping:"postShow", params:post.encodeAsLinkProperties()

        }
    }


    private static final boolean WITH_POST_REVIEW = false;
    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def review(){
        Post post = params.post
        if (post.owner.id != springSecurityService.principal.id){
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
            return;
        }
        [post:post]
        //TODO: EL proceso de review no queda claro y la gente no llega a publicar. Solo salva. Se hace de modo r�pido
        if (!WITH_POST_REVIEW){
            redirect mapping:"postPublish", params:post.encodeAsLinkProperties()
        }
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def publish(){
        Post post = params.post
        if (post.owner.id != springSecurityService.principal.id){
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
            return;
        }
        postService.publishPost(post)
        flash.args = [justPublished:true]
        redirect mapping:"postPublished", params:post.encodeAsLinkProperties()
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def deletePost(){
        Post post = params.post
        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        if (!postService.isDeletableByUser(post, user)){
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
            return;
        }
        postService.deletePost(post, user)
        flash.message=message(code:'profile.profileMyPosts.post.success')
        redirect( mapping:'toolsMyPosts')
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def postPublished(){
        Post post = params.post
        if (post.owner.id != springSecurityService.principal.id){
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
            return;
        }
        def model = [post:post]
        if (flash.args?.justPublished){
            def numVotesToBePublic = grailsApplication.config.kuorum.milestones.postVotes.publicVotes
            def gamification = [
                    title: "${message(code:'post.edit.step3.gamification.title')}",
                    text:"${message(code:'post.edit.step3.gamification.motivationText', args:[numVotesToBePublic])}",
                    eggs:gamificationService.gamificationConfigCreatePost()[GamificationElement.EGG]?:0,
                    plumes:gamificationService.gamificationConfigCreatePost()[GamificationElement.PLUME]?:0,
                    corns:gamificationService.gamificationConfigCreatePost()[GamificationElement.CORN]?:0
            ]
            model += [gamificationData:gamification]
        }
        model
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def promotePost(){
        Post post = params.post
        def promotionPrizes = grailsApplication.config.kuorum.post.promotionPrizes
        def prices = promotionPrizes.collect{[price:it, numMails: postService.calculateNumEmails(it)]}
        [post:post, prices:prices, command: new PromotePostCommand()]
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def paimentPost(PromotePostCommand command){
        Post post = params.post
        if (command.hasErrors()){
            def promotionPrizes = grailsApplication.config.kuorum.post.promotionPrizes
            def prices = promotionPrizes.collect{[price:it, numMails: postService.calculateNumEmails(it)]}
            [post:post, prices:prices, command: command]
        }

        [post:post, amount:command.amount, numMails:postService.calculateNumEmails(command.amount)]
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def successPromotePost(PromotePostCommand command){
        Post post = params.post
        [post:post, numMails: 1000]
    }


    @Secured('IS_AUTHENTICATED_REMEMBERED')
    def favorite() {
        Post post = params.post
        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        if (user.favorites.contains(post.id)){
            postService.favoriteRemovePost(post,user)
            response.setHeader(WebConstants.AJAX_IS_FAVORITE, "false")
            response.setHeader(WebConstants.AJAX_NUM_LIST, "${user.favorites.size()}")
            render "Deleted from favorites"
        }else{
            postService.favoriteAddPost(post,user)
            response.setHeader(WebConstants.AJAX_IS_FAVORITE, "true")
            response.setHeader(WebConstants.AJAX_NUM_LIST, "${user.favorites.size()}")
            render template: "/cluck/liCluck", model: [post:post]
        }

    }

    @Secured('IS_AUTHENTICATED_REMEMBERED')
    def deleteComment(Integer commentPosition){
        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        Post post = params.post
        if (postService.isCommentDeletableByUser(user, post, commentPosition)){
            postService.deleteComment(user,post,commentPosition)
        }else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED)
            return;
        }

        render "Comment deleted"
    }

    @Secured('IS_AUTHENTICATED_REMEMBERED')
    def addComment(CommentPostCommand command){
        if (command.hasErrors()){
            render "EMpty comment"
        }else{
            Post post = params.post
            KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
            PostComment postComent = new PostComment(kuorumUser: user, text:command.comment)
            postComent = postService.addComment(params.post, postComent)
            render template: '/post/postComment', model:[post:post, comment:postComent, pos:post.comments.size()-1, display:'none']
        }
    }
    @Secured('IS_AUTHENTICATED_REMEMBERED')
    def voteComment(Integer commentPosition){
        VoteType voteType = VoteType.valueOf(params['voteType'])
        Post post = params.post
        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        postService.voteComment(user, post, commentPosition, voteType);
        render "Post voted"
    }


    @Secured('isAuthenticated()')
    def cluckPost() {
        KuorumUser kuorumUser = KuorumUser.get(springSecurityService.principal.id)
        Post post = params.post
        Cluck cluck = cluckService.createCluck(post, kuorumUser)
        //TODO: que se renderiza
        render "OK"
    }

    @Secured('isAuthenticated()')
    def votePost() {
        KuorumUser kuorumUser = KuorumUser.get(springSecurityService.principal.id)
        Boolean anonymous = params.anonymous?Boolean.valueOf(params.anonymous):Boolean.FALSE
        Post post = params.post
        postVoteService.votePost(post, kuorumUser, anonymous)
        Range<Long> range = postVoteService.findPostRange(post)
        def gamification = [
                title: "${message(code:'post.show.boxes.like.vote.gamification.title', args:[post.owner.name])}",
                text:"${message(code:'post.show.boxes.like.vote.gamification.motivationText', args:[post.owner.name])}",
                eggs:gamificationService.gamificationConfigVotePost()[GamificationElement.EGG]?:0,
                plumes:gamificationService.gamificationConfigVotePost()[GamificationElement.PLUME]?:0,
                corns:gamificationService.gamificationConfigVotePost()[GamificationElement.CORN]?:0
        ]
        render ([numLikes:post.numVotes, limitTo:range.to +1, gamification:gamification] as JSON)
    }

    def votePostWithRegister(KuorumRegisterCommand command) {
        if (command.hasErrors()) {
            render view: 'index', model: [command: command]
            return redirect(mapping: 'register', params: command.properties)
        }
        Post post = params.post
        Boolean anonymous = params.anonymous?Boolean.valueOf(params.anonymous):Boolean.FALSE
        KuorumUser user = registerService.registerUserVotingPost(command, post, anonymous)
//        String referer = request.getHeader("Referer");
//        response.sendRedirect(referer);
        redirect mapping:"postShow", params: post.encodeAsLinkProperties()
    }

    def listVotes(){
        Post post = params.post
        List<KuorumUser> users = postVoteService.findVotedUsers(post, new Pagination())
        render (template:'/kuorumUser/embebedUsersList', model:[users:users])
    }

    def listClucks(){
        Post post = params.post
        List<KuorumUser> users = cluckService.findPostCluckers(post, new Pagination())
        render (template:'/kuorumUser/embebedUsersList', model:[users:users])
    }

    @Secured('isAuthenticated()')
    def addDebate(CommentPostCommand command) {
        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        Post post = params.post
        if (command.hasErrors() || !postService.isAllowedToAddDebate(post, user) ){
            render "Something wrong. Not possible to arrive here using web normally"
        }else{
            PostComment postComment = new PostComment(
                    text: command.comment,
                    dateCreated:new Date(),
                    moderated: Boolean.FALSE,
                    deleted :Boolean.FALSE,
                    kuorumUser: user
            )
            postService.addDebate(post, postComment)
            postComment = post.debates.last()
            if (user.userType == UserType.POLITICIAN){
                render (template: '/post/debates/postDebatePolitician', model:[debate:postComment])
            }else{
                render (template: '/post/debates/postDebateActivist', model:[debate:postComment])
            }
        }
    }
    @Secured("ROLE_POLITICIAN")
    def addDefender(PostDefendCommand command) {
        if (command.hasErrors()){
            flash.error = message(code:'modalDefend.error')
            redirect (mapping:"postShow", params:post.encodeAsLinkProperties())
            return;
        }
        Post post = params.post
        KuorumUser politician = KuorumUser.get(springSecurityService.principal.id)
        postService.defendPost(post, politician, command.text)
        flash.message = message(code:'modalDefend.success')
        redirect (mapping:"postShow", params:post.encodeAsLinkProperties())
    }

    @Secured('isAuthenticated()')
    def addVictory(Boolean victoryOk) {
        KuorumUser user = KuorumUser.get(springSecurityService.principal.id)
        Post post = params.post
        postService.victory(post, user, victoryOk)
//        flash.message = message(code:'modalVictory.success', args:[post.defender.name])
//        redirect (mapping:"postShow", params:post.encodeAsLinkProperties())
        render message(code:'modalVictory.success', args:[post.defender.name])
    }
}
