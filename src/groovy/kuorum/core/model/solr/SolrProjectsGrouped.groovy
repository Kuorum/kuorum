package kuorum.core.model.solr

import kuorum.core.model.CommissionType

/**
 * Created by iduetxe on 7/05/14.
 */
class SolrProjectsGrouped {

    CommissionType commission
    List<SolrProject> elements
}
