package kuorum.core.model.solr

import kuorum.core.model.CommissionType

/**
 * Created by iduetxe on 17/02/14.
 */
class SolrProject extends SolrElement{
    String hashtag
    String text
    String owner
    String ownerId
    Date deadLine
    Integer relevance
}
