package kuorum.core.model.kuorumUser

import kuorum.project.Project

/**
 * Represents the participation of an user on a project
 */
class UserParticipating {
    Project project
    Long numTimes
}
