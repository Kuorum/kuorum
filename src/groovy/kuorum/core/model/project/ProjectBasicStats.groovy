package kuorum.core.model.project

/**
 * Created by iduetxe on 28/04/14.
 */
class ProjectBasicStats {
    Integer numUsers
    Double percentagePositiveVotes
    Double percentageNegativeVotes
    Double percentageAbsVotes
    Date lastActivity
    Integer numPublicPosts
    Integer nomVotesToBePublic
    Long visits
}
