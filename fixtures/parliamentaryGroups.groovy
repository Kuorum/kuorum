import kuorum.PoliticalParty

fixture {

    grupoSocialista (PoliticalParty, name :"Partido Socialista"          )
    grupoPopular    (PoliticalParty, name :"Partido Popular"             )
    grupoVasco      (PoliticalParty, name :"Grupo Vasco"                 )
    grupoCatalan    (PoliticalParty, name :"Grupo Catalán"               )
    grupoMixto      (PoliticalParty, name :"Grupo Mixto"                 )
    grupoIzqPlural  (PoliticalParty, name :"Grupo de la Izquierda Plural")
    grupoUPyD       (PoliticalParty, name :"Grupo UPyD"                  )

}
