package kuorum.users

import groovyx.gpars.GParsPool
import org.bson.types.ObjectId
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by iduetxe on 17/03/14.
 */
class KuorumUserServiceIntegrationTest extends Specification {

    def kuorumUserService
    def fixtureLoader

    def setup() {
        KuorumUser.collection.getDB().dropDatabase()
        fixtureLoader.load("testBasicData")
    }

    void "test creating a follower"() {
        given: "2 users"
        KuorumUser follower = KuorumUser.findByEmail(followerEmail)
        KuorumUser following = KuorumUser.findByEmail(followingEmail)
        Integer futureNumFollowers = following.numFollowers
        if (!follower.following.contains(following.id)) {
            futureNumFollowers++
        }

        when: "Adding a follower"
        //"service" represents the grails service you are testing for
        kuorumUserService.createFollower(follower, following)
        then: "All OK"
        follower.following.contains(following.id)
        following.followers.contains(follower.id)
        futureNumFollowers == following.numFollowers
        KuorumUser.withNewSession {
            KuorumUser followerNS = KuorumUser.findByEmail(followerEmail)
            KuorumUser followingNS = KuorumUser.findByEmail(followingEmail)
            followerNS.following.contains(followingNS.id)
            followingNS.followers.contains(followerNS.id)
            futureNumFollowers == followingNS.numFollowers
        }
        where:
        followerEmail       | followingEmail
        "peter@example.com" | "equo@example.com"
        "peter@example.com" | "carmen@example.com"
    }

    @Unroll
    void "test deleting a follower"() {
        given: "2 users"
        KuorumUser follower = KuorumUser.findByEmail(followerEmail)
        KuorumUser following = KuorumUser.findByEmail(followingEmail)

        if (createFollower)
            kuorumUserService.createFollower(follower, following)

        Integer futureNumFollowers = following.numFollowers
        if (follower.following.contains(following.id)) {
            futureNumFollowers--
        }

        when: "Deleting follower"
        //"service" represents the grails service you are testing for
        kuorumUserService.deleteFollower(follower, following)
        then: "All OK"
        !follower.following.contains(following.id)
        !following.followers.contains(follower.id)
        futureNumFollowers == following.numFollowers
        KuorumUser.withNewSession {
            KuorumUser followerNS = KuorumUser.findByEmail(followerEmail)
            KuorumUser followingNS = KuorumUser.findByEmail(followingEmail)
            !followerNS.following.contains(followingNS.id)
            !followingNS.followers.contains(followerNS.id)
            futureNumFollowers == followingNS.numFollowers
        }
        where:
        followerEmail       | followingEmail       | createFollower
        "peter@example.com" | "equo@example.com"   | false
        "peter@example.com" | "carmen@example.com" | true
    }

    @Unroll
    void "test converting as premium on #email"() {
        given: "1 users"
        KuorumUser user = KuorumUser.findByEmail(email)
        when:
        kuorumUserService.convertAsPremium(user)
        then:
        user.authorities.collect { it.authority }.contains("ROLE_PREMIUM")
        user.authorities.size() == 2
        KuorumUser.withNewSession {
            KuorumUser userNS = KuorumUser.findByEmail(email)
            userNS.authorities.collect { it.authority }.contains("ROLE_PREMIUM")
            userNS.authorities.size() == 2
        }
        where:
        email                      | result
        "peter@example.com"        | true  //He has not the role yet
        "juanjoalvite@example.com" | true  // He has already the role
    }

    @Unroll
    void "test deleting premium roles on user #email"() {
        given: "1 premium users"
        KuorumUser user = KuorumUser.findByEmail(email)
        when:
        kuorumUserService.convertAsNormalUser(user)
        then:
        !user.authorities.collect { it.authority }.contains("ROLE_PREMIUM")
        user.authorities.size() == 1
        KuorumUser.withNewSession {
            KuorumUser userNS = KuorumUser.findByEmail(email)
            !userNS.authorities.collect { it.authority }.contains("ROLE_PREMIUM")
            userNS.authorities.size() == 1
        }
        where:
        email                      | result
        "peter@example.com"        | true  //He has not the role yet
        "juanjoalvite@example.com" | true  // He has already the role

    }

    void "Calculate the activity between two users"() {
        given: "A user"
        KuorumUser user = KuorumUser.findByEmail('politician@example.com')

        and: "All users, except the created user"
        KuorumUser compareUser = KuorumUser.findByEmail('carmen@example.com')

        when: "Calculate the activity between the users"
        Integer activity = kuorumUserService.calculateActivityClosure.call(user, compareUser)

        then: "The expected list and the obtained recommended user are equals"
        activity == 151
    }

    @Unroll
    void "Calculate the recommended users by Facebook friends"() {
        given: "A user"
        KuorumUser user = KuorumUser.findByEmail('politician@example.com')

        and: "A recommendeUserInfo for the user"
        RecommendedUserInfo recommendedUserInfo = new RecommendedUserInfo(
                user: user,
                recommendedUsers: [KuorumUser.findByEmail('carmen@example.com').id]
        ).save(flush: true)

        and: "A mock list of facebook friends"
        Map facebookFriends = [data: []]

        emailsFacebookFriends.each { String email ->
            long id
            GParsPool.withPool {
                id = email.bytes.sumParallel()
            }
            facebookFriends.data << [id: id]
        }

        and: "A mock list of FacebookUsers"
        List<FacebookUser> facebookUsers = []
        emailsFacebookUsers.each { String email ->
            long id
            GParsPool.withPool {
                id = email.bytes.sumParallel()
            }
            facebookUsers << new FacebookUser(
                    user: KuorumUser.findByEmail(email),
                    accessToken: 'accesToken',
                    id: KuorumUser.findByEmail(email).id,
                    accessTokenExpires: new Date() + 1,
                    uid: id
            ).save(flush: true)
        }
        //Change the maxSize constraint only for test mode
        RecommendedUserInfo.constraints.recommendedUsers.maxSize = 2

        when: "Calculate the recommended users by Facebook friends"
        List<ObjectId> recommendedUsersByFacebookFriends = kuorumUserService.recommendedUsersByFacebookFriends(user, facebookUsers, facebookFriends, recommendedUserInfo)

        then: "The expected list and the obtained recommended user are equals"
        recommendedUsersByFacebookFriends.size() == resultSize

        cleanup:
        recommendedUserInfo.delete()
        facebookUsers*.delete()

        where:
        emailsFacebookFriends                                                | emailsFacebookUsers                                                  || resultSize
        ['ecologistas@example.com']                                          | ['ecologistas@example.com']                                          || 2
        ['newuser@example.com', 'equo@example.com']                          | ['ecologistas@example.com', 'equo@example.com']                      || 2
        ['noe@example.com', 'peter@example.com', 'juanjoalvite@example.com'] | ['ecologistas@example.com', 'equo@example.com']                      || 0
        ['noe@example.com', 'peter@example.com', 'juanjoalvite@example.com'] | ['noe@example.com', 'peter@example.com', 'juanjoalvite@example.com'] || 2
        []                                                                   | []                                                                   || 0
        ['carmen@example.com']                                               | ['carmen@example.com']                                               || 0
        ['carmen@example.com','ecologistas@example.com']                     | ['carmen@example.com','ecologistas@example.com']                     || 2
    }
}
